#! /bin/python3
import datetime
import yaml
from dateutil.relativedelta import relativedelta
import sys


class main(object):
    def color(self):
        if self.thisday == self.today:
            return "${color " + self.config["color"]["today"].replace("#","") + "}"
        elif self.thisday.strftime("%a") == "Sat" or self.thisday.strftime("%a") == "Sun":
            return "${color " + self.config["color"]["weekend"].replace("#","") + "}"
        else:
            return "${color " + self.config["color"]["workday"].replace("#","") + "}"

    def thisday(self):
        try:
            return (self.today+relativedelta(months=(int(sys.argv[2])))).replace(day=int(sys.argv[1]))
        except:
            sys.exit()

    def note(self):
        with open('note.txt', 'r') as seznam:
            for radek in seznam:
                radek = radek.rstrip()
                if (radek.find(self.thisday.strftime("%d.%m."))) != -1:
                    return radek[7:35]
            return " "

    def __init__(self):
        self.today = datetime.date.today()
        self.thisday = self.thisday()
        with open('config.yml', 'r') as configFile:
            self.config = yaml.load(configFile)

if __name__ == "__main__":
    main = main()
    print(main.color(), main.thisday.strftime("%d.%m."), main.note())
