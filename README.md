# Conky-calendar
Simple calendar to desktop based on conky and python script

## Screenshot
![screenshot](https://github.com/frimdo/Conky-calendar/blob/master/screenshot.png)

## Requirements
[Python3](https://wiki.python.org/moin/BeginnersGuide/Download)

[python-dateutil](https://pypi.python.org/pypi/python-dateutil)

[conky](https://github.com/brndnmtthws/conky)

# How to install 
```
git clone https://github.com/frimdo/Conky-calendar
```
To launch use
```
./start.sh
```
To update after changes in note.txt file
```
./update.sh
```

# How to use
Add notes to note.txt 

For example:
```
20.06. First note
28.12. Second note
03.04. Another note
Line with bad date format or without any will not show
20.06. This note will not be shown, there is another with same date higher
24.12. Christmass
```

# How to configure
Only configuration of colors is available. To change colors, just edit config.yml file.
